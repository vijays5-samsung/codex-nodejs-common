import { ArgumentMetadata, Injectable, PipeTransform } from "@nestjs/common";

@Injectable()
export class CsvPipe implements PipeTransform {
  public transform(value: any, metadata: ArgumentMetadata): any {
    return value.split(",");
  }
}
