export class ArrayUtil {
  static paginate(array, pageNumber, pageSize) {
    // human-readable page numbers usually start with 1, so we reduce 1 in the first argument
    return array.slice((pageNumber - 1) * pageSize, pageNumber * pageSize);
  }
  static unique(array) {
    return array.filter((v, i, a) => a.indexOf(v) === i);
  }
  static intersection(array1, array2) {
    return array1.filter((value) => array2.includes(value));
  }
}
