import { ApiQuery } from "@nestjs/swagger";
import { applyDecorators } from "@nestjs/common";

export function ApiPagination() {
  return applyDecorators(
    ...[
      ApiQuery({
        name: "page",
        required: true,
        example: 1,
      }),
      ApiQuery({
        name: "pageSize",
        required: true,
        example: 10,
      }),
    ]
  );
}
