import { ArgumentMetadata, PipeTransform } from "@nestjs/common";
export declare class LanguageCodePipe implements PipeTransform {
    transform(value: any, metadata: ArgumentMetadata): any;
}
