import { ClientInterface, LocaleInterface, MessageContextInterface, MessageMetaInterface, MetaTimeInterface } from "@cryptexlabs/codex-data-model";
import { Context } from "../context";
export declare class MessageMetaFromMeta implements MessageMetaInterface {
    readonly type: string;
    readonly client: ClientInterface;
    readonly context: MessageContextInterface;
    readonly correlationId: string;
    readonly locale: LocaleInterface;
    readonly schemaVersion: "0.1.0";
    readonly time: MetaTimeInterface;
    constructor(context: Context, meta: MessageMetaInterface, type: string);
}
