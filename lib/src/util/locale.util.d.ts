import { Locale } from "@cryptexlabs/codex-data-model";
import { Context } from "../context";
export declare class LocaleUtil {
    private constructor();
    static getLocaleFromHeaders(headers: Record<string, string>, context: Context): Locale;
}
