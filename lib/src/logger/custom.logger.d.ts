import { LoggerService } from "@nestjs/common";
export declare class CustomLogger implements LoggerService {
    private defaultContext;
    private readonly logger;
    private readonly filterContext;
    constructor(defaultContext?: string, filterContext?: string[], isTimestampEnabled?: boolean, logger?: LoggerService);
    log(message: string, context?: string): void;
    error(error: string | Error, trace: string, context?: string): void;
    debug(message: any, context?: string): any;
    verbose(message: any, context?: string): any;
    warn(message: any, context?: string): any;
    info(message: any, context?: string): any;
}
