"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.HttpAuthzAttachObjectsGuardUtil = void 0;
const http_authz_action_to_sub_objects_guard_util_1 = require("./http-authz.action-to-sub-objects.guard.util");
class HttpAuthzAttachObjectsGuardUtil {
    constructor(context) {
        this.context = context;
        this._util = new http_authz_action_to_sub_objects_guard_util_1.HttpAuthzActionToSubObjectsGuardUtil(context, "create");
    }
    isAuthorized(object, objectId, attachObject, attachObjectIds, namespace) {
        return this._util.isAuthorized(object, objectId, attachObject, attachObjectIds, namespace);
    }
    get params() {
        return this._util.params;
    }
    get query() {
        return this._util.query;
    }
    get body() {
        return this._util.body;
    }
}
exports.HttpAuthzAttachObjectsGuardUtil = HttpAuthzAttachObjectsGuardUtil;
//# sourceMappingURL=http-authz.attach-objects.guard.util.js.map