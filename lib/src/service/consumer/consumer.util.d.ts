export declare class ConsumerUtil {
    private constructor();
    static getTopics(topics: string[]): (RegExp | string)[];
}
