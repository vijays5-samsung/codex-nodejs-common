import { ElasticsearchService } from "@nestjs/elasticsearch";
import { CustomLogger } from "../../logger";
export declare class ElasticsearchInitializer {
    private readonly logger;
    private readonly index;
    private readonly elasticsearchService;
    private readonly indexProperties;
    private readonly type?;
    constructor(logger: CustomLogger, index: string, elasticsearchService: ElasticsearchService, indexProperties: any, type?: string);
    private _createIndex;
    private _getIndexExists;
    initializeIndex(): Promise<void>;
    _waitForReady(): Promise<void>;
    _isReady(): Promise<boolean>;
}
