import { WebsocketBroadcasterInterface } from "./websocket-broadcaster.interface";
import { SocketSubscribedClient } from "./socket-subscribed-client";
export declare class BroadcasterService implements WebsocketBroadcasterInterface {
    subscribedClients: SocketSubscribedClient[];
    broadcast(topic: any, message: any): void;
    getAllTopicsForClients(): any[];
}
