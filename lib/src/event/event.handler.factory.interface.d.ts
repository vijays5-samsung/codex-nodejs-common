import { EventHandlerInterface } from "./event.handler.interface";
export interface EventHandlerFactoryInterface {
    getHandler(topic: string): EventHandlerInterface;
}
